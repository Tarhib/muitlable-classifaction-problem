# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os
import csv
import pandas as pd
import numpy as np


data_path = "senti_data.csv"

data_raw = pd.read_csv(data_path,error_bad_lines=False,encoding = "ISO-8859-1")
close_data=data_raw['Sentiment']

i=0
numbers =[1,521,851,1227,4153,4861,5509,7700,10500,12201,13901,15201,15901,16401,18401,20501,24001,27101,29401,35601]
j=150

for i in range(len(numbers)):
   for k in range(numbers[i],numbers[i]+j):
       if(np.isnan(data_raw['Sentiment'][k])):
           data_raw['Sentiment'][k]=0
   
